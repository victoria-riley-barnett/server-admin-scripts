# server admin scripts

Server scripts for managing updates and services

## Getting started

Log in to the server to be managed and clone this repo.

```
cd
git clone https://gitlab.com/DSASanFrancisco/server-admin-scripts.git
```

## Update server admin scripts

```
cd /server-admin-scripts
git pull
```

