#!/bin/bash

echo "NOTE: this upgrade script will take a few minutes"
sleep 10
echo "starting discourse upgrade"
cd /var/discourse
git pull
./launcher rebuild app

echo "discourse upgraded"